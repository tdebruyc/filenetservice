package gov.mn.commerce.efiling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfilingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfilingApplication.class, args);
	}

}
