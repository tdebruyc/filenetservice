package gov.mn.commerce.efiling.filenet;

import lombok.Data;
import lombok.extern.log4j.Log4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dstewart on 4/28/2017.
 */
@Log4j
@Data
public class FilenetPage implements Serializable {

    private boolean isNext;
    private boolean isPrevious;
    private Integer currentPage;
    private Integer pageSize;
    private Integer totalResultSize;
    private List<CommerceFilenetDocument> filenetDocumentList = new ArrayList<CommerceFilenetDocument>();
    private FileNetError info;

    public FilenetPage(boolean isNext, boolean isPrevious, Integer currentPage, Integer pageSize, Integer totalResultSize, List<CommerceFilenetDocument> filenetDocumentList) {
        this.isNext = isNext;
        this.isPrevious = isPrevious;
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalResultSize = totalResultSize;
        this.filenetDocumentList = filenetDocumentList;
    }

    public FilenetPage(final Exception e) {
        String logMessage = "An error occurred related to the information provided to FileNet. Exception message: " + e.getMessage();
        log.error(logMessage);
        info = new FileNetError();
        info.setError(true);
        info.setMessage(logMessage);
    }

    public FilenetPage() {
    }


}
