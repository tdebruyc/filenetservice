package gov.mn.commerce.efiling.controller;

import java.io.Serializable;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gov.mn.commerce.efiling.filenet.DatabaseProperties;
import gov.mn.commerce.efiling.filenet.DynamicFilenetService;
import gov.mn.commerce.efiling.filenet.FilenetPage;
import gov.mn.commerce.efiling.util.JSONDataUtil;
import lombok.extern.log4j.Log4j;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/filenet")
@Log4j
public class FilenetRestController implements Serializable {

	private static final long serialVersionUID = 2197174920459872721L;
	private DynamicFilenetService dynamicFilenetService;
	
    @CrossOrigin(origins = "http://localhost:8192")
	@RequestMapping(value="/dump", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage dumpprops() {
		log.debug("##### hitting dump");
		Properties props = DatabaseProperties.getDbProperties();

		JSONDataUtil.printAsJSON("Props", props);
		String url = GetValue("filenet.url.dev", props);
		String userName = GetValue("filenet.dockets.username.dev", props);
		String password = GetValue("filenet.dockets.password.dev", props);	
		String query = "SELECT TOP 100 DocumentDate,ReceivedDate,DocketID,OnBehalfof,AdditionalInfo,Name,Id,DocumentTitle,MimeType,DocumentType FROM DocketsPublic d WHERE d.[DocketID] = '17-1'";
			
		return queryFilenet(url, userName, password, query, 1, 10);
	}
	
    public String GetValue(String key, Properties properties) {
        return properties.getProperty(key);
    }
	
	@RequestMapping(value = "/query", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage queryFilenet(String url, String username, String password, String sql,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize) {
		dynamicFilenetService.queryFilenet(url, username, password, sql, page, pageSize);
		return dynamicFilenetService.getFilenetPage();
	}

	@RequestMapping(value = "/download", method = RequestMethod.POST, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void downloadDocument(String url, String username, String password, String docId,
			HttpServletResponse response) {
		dynamicFilenetService.downloadDocumentStream(url, username, password, docId, response);
	}

	@PostConstruct
	public void postConstruct() {

	}

	@Autowired
	public FilenetRestController(DynamicFilenetService dynamicFilenetService) {
		this.dynamicFilenetService = dynamicFilenetService;
	}
}
