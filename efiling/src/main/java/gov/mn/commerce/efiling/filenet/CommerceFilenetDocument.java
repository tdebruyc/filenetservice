package gov.mn.commerce.efiling.filenet;

import com.filenet.api.property.Properties;
import com.filenet.api.property.Property;
import com.filenet.api.query.RepositoryRow;
import lombok.Data;

import java.io.InputStream;
import java.io.Serializable;
import java.util.*;


/**
 * Created by dstewart on 3/17/2016.
 */

@Data
public class CommerceFilenetDocument implements Serializable {

    private Map<String, Object> docPropertyMap = new LinkedHashMap<String, Object>();

    public List<Map.Entry<String,Object>> getDocPropertyList(){
        return new ArrayList<Map.Entry<String,Object>>(docPropertyMap.entrySet());
    }

    private InputStream docStream;

    CommerceFilenetDocument() {
    }

    CommerceFilenetDocument(RepositoryRow document) {
        Properties props = document.getProperties();
        Iterator i = props.iterator();
        while (i.hasNext()) {
            Property p = (Property) i.next();
            determinePropertyType(p);
        }
    }

    private void determinePropertyType(Property p) {
        String type = p.getClass().getName();
        String key = p.getPropertyName();
        if (type.equalsIgnoreCase("com.filenet.apiimpl.property.PropertyIdImpl")) {
            docPropertyMap.put(key, p.getIdValue().toString());
        }else {
            docPropertyMap.put(key,p.getObjectValue());
        }
    }

}
