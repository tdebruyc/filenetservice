package gov.mn.commerce.efiling.filenet;

import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.DatabaseConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.Serializable;
import java.util.Properties;

/**
 * Created by dstewart on 4/27/2017.
 */

@Component
public class DatabaseProperties implements Serializable{

    @Autowired
    private DataSource eeraSource;

    private static Properties dbProperties;

    public static Properties getDbProperties() {
        return dbProperties;
    }

    public void loadFromDatabase(){
        DatabaseConfiguration config = new DatabaseConfiguration(eeraSource,"eera_setting","setting_key","setting_value");
        dbProperties = ConfigurationConverter.getProperties(config);
    }

    @PostConstruct
    public void postConstruct(){
        loadFromDatabase();
    }
}
