package gov.mn.commerce.efiling.filenet;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by dstewart on 5/15/2017.
 */

@Data
public class FileNetError implements Serializable {

    boolean isError;
    String message;
}
