package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the edi_ds_details database table.
 * 
 */
@Entity
@Table(name="edi_ds_details")
@NamedQuery(name="EdiDsDetail.findAll", query="SELECT e FROM EdiDsDetail e")
public class EdiDsDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_DS_DETAILS_DSDETAILID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_DS_DETAILS_DSDETAILID_GENERATOR")
	@Column(name="DS_DETAIL_ID")
	private long dsDetailId;

	@Column(name="DS_KEY")
	private String dsKey;

	@Column(name="DS_VALUE")
	private String dsValue;

	//bi-directional many-to-one association to EdiDatasource
	@ManyToOne
	@JoinColumn(name="DS_ID")
	private EdiDatasource ediDatasource;

	public EdiDsDetail() {
	}

	public long getDsDetailId() {
		return this.dsDetailId;
	}

	public void setDsDetailId(long dsDetailId) {
		this.dsDetailId = dsDetailId;
	}

	public String getDsKey() {
		return this.dsKey;
	}

	public void setDsKey(String dsKey) {
		this.dsKey = dsKey;
	}

	public String getDsValue() {
		return this.dsValue;
	}

	public void setDsValue(String dsValue) {
		this.dsValue = dsValue;
	}

	public EdiDatasource getEdiDatasource() {
		return this.ediDatasource;
	}

	public void setEdiDatasource(EdiDatasource ediDatasource) {
		this.ediDatasource = ediDatasource;
	}

}