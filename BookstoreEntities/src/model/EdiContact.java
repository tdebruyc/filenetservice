package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the edi_contacts database table.
 * 
 */
@Entity
@Table(name="edi_contacts")
@NamedQuery(name="EdiContact.findAll", query="SELECT e FROM EdiContact e")
public class EdiContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_CONTACTS_CONTACTID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_CONTACTS_CONTACTID_GENERATOR")
	@Column(name="CONTACT_ID")
	private long contactId;

	@Column(name="CONTACT_EMAIL")
	private String contactEmail;

	@Column(name="CONTACT_NAME")
	private String contactName;

	//bi-directional many-to-many association to EdiQuery
	@ManyToMany(mappedBy="ediContacts")
	private List<EdiQuery> ediQueries;

	public EdiContact() {
	}

	public long getContactId() {
		return this.contactId;
	}

	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	public String getContactEmail() {
		return this.contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactName() {
		return this.contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public List<EdiQuery> getEdiQueries() {
		return this.ediQueries;
	}

	public void setEdiQueries(List<EdiQuery> ediQueries) {
		this.ediQueries = ediQueries;
	}

}