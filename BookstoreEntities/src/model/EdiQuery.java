package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the edi_queries database table.
 * 
 */
@Entity
@Table(name="edi_queries")
@NamedQuery(name="EdiQuery.findAll", query="SELECT e FROM EdiQuery e")
public class EdiQuery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_QUERIES_TASKID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_QUERIES_TASKID_GENERATOR")
	@Column(name="TASK_ID")
	private long taskId;

	@Column(name="DEST_QUERY")
	private String destQuery;

	@Column(name="EXECUTE_ORDER")
	private int executeOrder;

	@Column(name="SOURCE_QUERY")
	private String sourceQuery;

	@Column(name="TASK_ACTIVE")
	private boolean taskActive;

	@Column(name="TASK_DESCRIP")
	private String taskDescrip;

	@Column(name="TASK_NAME")
	private String taskName;

	//bi-directional many-to-one association to EdiDatasource
	@ManyToOne
	@JoinColumn(name="SOURCE_CONN_ID")
	private EdiDatasource ediDatasource1;

	//bi-directional many-to-one association to EdiDatasource
	@ManyToOne
	@JoinColumn(name="DEST_CONN_ID")
	private EdiDatasource ediDatasource2;

	//bi-directional many-to-one association to EdiProcess
	@ManyToOne
	@JoinColumn(name="PROCESS_ID")
	private EdiProcess ediProcess;

	//bi-directional many-to-many association to EdiContact
	@ManyToMany
	@JoinTable(
		name="edi_queries_contacts"
		, joinColumns={
			@JoinColumn(name="TASK_ID")
			}
		, inverseJoinColumns={
			@JoinColumn(name="CONTACT_ID")
			}
		)
	private List<EdiContact> ediContacts;

	public EdiQuery() {
	}

	public long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getDestQuery() {
		return this.destQuery;
	}

	public void setDestQuery(String destQuery) {
		this.destQuery = destQuery;
	}

	public int getExecuteOrder() {
		return this.executeOrder;
	}

	public void setExecuteOrder(int executeOrder) {
		this.executeOrder = executeOrder;
	}

	public String getSourceQuery() {
		return this.sourceQuery;
	}

	public void setSourceQuery(String sourceQuery) {
		this.sourceQuery = sourceQuery;
	}

	public boolean getTaskActive() {
		return this.taskActive;
	}

	public void setTaskActive(boolean taskActive) {
		this.taskActive = taskActive;
	}

	public String getTaskDescrip() {
		return this.taskDescrip;
	}

	public void setTaskDescrip(String taskDescrip) {
		this.taskDescrip = taskDescrip;
	}

	public String getTaskName() {
		return this.taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public EdiDatasource getEdiDatasource1() {
		return this.ediDatasource1;
	}

	public void setEdiDatasource1(EdiDatasource ediDatasource1) {
		this.ediDatasource1 = ediDatasource1;
	}

	public EdiDatasource getEdiDatasource2() {
		return this.ediDatasource2;
	}

	public void setEdiDatasource2(EdiDatasource ediDatasource2) {
		this.ediDatasource2 = ediDatasource2;
	}

	public EdiProcess getEdiProcess() {
		return this.ediProcess;
	}

	public void setEdiProcess(EdiProcess ediProcess) {
		this.ediProcess = ediProcess;
	}

	public List<EdiContact> getEdiContacts() {
		return this.ediContacts;
	}

	public void setEdiContacts(List<EdiContact> ediContacts) {
		this.ediContacts = ediContacts;
	}

}