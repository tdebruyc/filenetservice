package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the edi_ds_type database table.
 * 
 */
@Entity
@Table(name="edi_ds_type")
@NamedQuery(name="EdiDsType.findAll", query="SELECT e FROM EdiDsType e")
public class EdiDsType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_DS_TYPE_DSTYPEID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_DS_TYPE_DSTYPEID_GENERATOR")
	@Column(name="DS_TYPE_ID")
	private long dsTypeId;

	@Column(name="DS_TYPE_NAME")
	private String dsTypeName;

	//bi-directional many-to-one association to EdiDatasource
	@OneToMany(mappedBy="ediDsType")
	private List<EdiDatasource> ediDatasources;

	public EdiDsType() {
	}

	public long getDsTypeId() {
		return this.dsTypeId;
	}

	public void setDsTypeId(long dsTypeId) {
		this.dsTypeId = dsTypeId;
	}

	public String getDsTypeName() {
		return this.dsTypeName;
	}

	public void setDsTypeName(String dsTypeName) {
		this.dsTypeName = dsTypeName;
	}

	public List<EdiDatasource> getEdiDatasources() {
		return this.ediDatasources;
	}

	public void setEdiDatasources(List<EdiDatasource> ediDatasources) {
		this.ediDatasources = ediDatasources;
	}

	public EdiDatasource addEdiDatasource(EdiDatasource ediDatasource) {
		getEdiDatasources().add(ediDatasource);
		ediDatasource.setEdiDsType(this);

		return ediDatasource;
	}

	public EdiDatasource removeEdiDatasource(EdiDatasource ediDatasource) {
		getEdiDatasources().remove(ediDatasource);
		ediDatasource.setEdiDsType(null);

		return ediDatasource;
	}

}