package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the edi_process database table.
 * 
 */
@Entity
@Table(name="edi_process")
@NamedQuery(name="EdiProcess.findAll", query="SELECT e FROM EdiProcess e")
public class EdiProcess implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_PROCESS_PROCESSID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_PROCESS_PROCESSID_GENERATOR")
	@Column(name="PROCESS_ID")
	private long processId;

	@Column(name="PROCESS_NAME")
	private String processName;

	//bi-directional many-to-one association to EdiQuery
	@OneToMany(mappedBy="ediProcess")
	private List<EdiQuery> ediQueries;

	public EdiProcess() {
	}

	public long getProcessId() {
		return this.processId;
	}

	public void setProcessId(long processId) {
		this.processId = processId;
	}

	public String getProcessName() {
		return this.processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public List<EdiQuery> getEdiQueries() {
		return this.ediQueries;
	}

	public void setEdiQueries(List<EdiQuery> ediQueries) {
		this.ediQueries = ediQueries;
	}

	public EdiQuery addEdiQuery(EdiQuery ediQuery) {
		getEdiQueries().add(ediQuery);
		ediQuery.setEdiProcess(this);

		return ediQuery;
	}

	public EdiQuery removeEdiQuery(EdiQuery ediQuery) {
		getEdiQueries().remove(ediQuery);
		ediQuery.setEdiProcess(null);

		return ediQuery;
	}

}