package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the edi_execution_log database table.
 * 
 */
@Entity
@Table(name="edi_execution_log")
@NamedQuery(name="EdiExecutionLog.findAll", query="SELECT e FROM EdiExecutionLog e")
public class EdiExecutionLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_EXECUTION_LOG_LOGID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_EXECUTION_LOG_LOGID_GENERATOR")
	@Column(name="LOG_ID")
	private long logId;

	@Column(name="FAILURE_MAIL_GROUP")
	private String failureMailGroup;

	@Column(name="LOG_CONFIGURATION")
	private String logConfiguration;

	@Column(name="LOG_RESULT")
	private String logResult;

	@Column(name="RUN_DT_TM")
	private String runDtTm;

	@Column(name="SUCCESS_MAIL_GROUP")
	private String successMailGroup;

	public EdiExecutionLog() {
	}

	public long getLogId() {
		return this.logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}

	public String getFailureMailGroup() {
		return this.failureMailGroup;
	}

	public void setFailureMailGroup(String failureMailGroup) {
		this.failureMailGroup = failureMailGroup;
	}

	public String getLogConfiguration() {
		return this.logConfiguration;
	}

	public void setLogConfiguration(String logConfiguration) {
		this.logConfiguration = logConfiguration;
	}

	public String getLogResult() {
		return this.logResult;
	}

	public void setLogResult(String logResult) {
		this.logResult = logResult;
	}

	public String getRunDtTm() {
		return this.runDtTm;
	}

	public void setRunDtTm(String runDtTm) {
		this.runDtTm = runDtTm;
	}

	public String getSuccessMailGroup() {
		return this.successMailGroup;
	}

	public void setSuccessMailGroup(String successMailGroup) {
		this.successMailGroup = successMailGroup;
	}

}