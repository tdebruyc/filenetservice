package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the edi_datasource database table.
 * 
 */
@Entity
@Table(name="edi_datasource")
@NamedQuery(name="EdiDatasource.findAll", query="SELECT e FROM EdiDatasource e")
public class EdiDatasource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EDI_DATASOURCE_DSID_GENERATOR", sequenceName="ORDER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EDI_DATASOURCE_DSID_GENERATOR")
	@Column(name="DS_ID")
	private long dsId;

	@Column(name="DS_NAME")
	private String dsName;

	//bi-directional many-to-one association to EdiDsType
	@ManyToOne
	@JoinColumn(name="DS_TYPE_ID")
	private EdiDsType ediDsType;

	//bi-directional many-to-one association to EdiDsDetail
	@OneToMany(mappedBy="ediDatasource")
	private List<EdiDsDetail> ediDsDetails;

	//bi-directional many-to-one association to EdiQuery
	@OneToMany(mappedBy="ediDatasource1")
	private List<EdiQuery> ediQueries1;

	//bi-directional many-to-one association to EdiQuery
	@OneToMany(mappedBy="ediDatasource2")
	private List<EdiQuery> ediQueries2;

	public EdiDatasource() {
	}

	public long getDsId() {
		return this.dsId;
	}

	public void setDsId(long dsId) {
		this.dsId = dsId;
	}

	public String getDsName() {
		return this.dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	public EdiDsType getEdiDsType() {
		return this.ediDsType;
	}

	public void setEdiDsType(EdiDsType ediDsType) {
		this.ediDsType = ediDsType;
	}

	public List<EdiDsDetail> getEdiDsDetails() {
		return this.ediDsDetails;
	}

	public void setEdiDsDetails(List<EdiDsDetail> ediDsDetails) {
		this.ediDsDetails = ediDsDetails;
	}

	public EdiDsDetail addEdiDsDetail(EdiDsDetail ediDsDetail) {
		getEdiDsDetails().add(ediDsDetail);
		ediDsDetail.setEdiDatasource(this);

		return ediDsDetail;
	}

	public EdiDsDetail removeEdiDsDetail(EdiDsDetail ediDsDetail) {
		getEdiDsDetails().remove(ediDsDetail);
		ediDsDetail.setEdiDatasource(null);

		return ediDsDetail;
	}

	public List<EdiQuery> getEdiQueries1() {
		return this.ediQueries1;
	}

	public void setEdiQueries1(List<EdiQuery> ediQueries1) {
		this.ediQueries1 = ediQueries1;
	}

	public EdiQuery addEdiQueries1(EdiQuery ediQueries1) {
		getEdiQueries1().add(ediQueries1);
		ediQueries1.setEdiDatasource1(this);

		return ediQueries1;
	}

	public EdiQuery removeEdiQueries1(EdiQuery ediQueries1) {
		getEdiQueries1().remove(ediQueries1);
		ediQueries1.setEdiDatasource1(null);

		return ediQueries1;
	}

	public List<EdiQuery> getEdiQueries2() {
		return this.ediQueries2;
	}

	public void setEdiQueries2(List<EdiQuery> ediQueries2) {
		this.ediQueries2 = ediQueries2;
	}

	public EdiQuery addEdiQueries2(EdiQuery ediQueries2) {
		getEdiQueries2().add(ediQueries2);
		ediQueries2.setEdiDatasource2(this);

		return ediQueries2;
	}

	public EdiQuery removeEdiQueries2(EdiQuery ediQueries2) {
		getEdiQueries2().remove(ediQueries2);
		ediQueries2.setEdiDatasource2(null);

		return ediQueries2;
	}

}