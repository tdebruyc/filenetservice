//package filenet;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.PostConstruct;
//import javax.servlet.http.HttpServletResponse;
//import java.io.Serializable;
//
///**
// * Created by dstewart on 11/17/2016.
// */
//
//@RestController
//@RequestMapping("/filenet")
//public class FilenetRestController implements Serializable {
//
//    private DynamicFilenetService dynamicFilenetService;
//
//    @RequestMapping(value ="/query",
//            method = RequestMethod.POST,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public FilenetPage queryFilenet(String url, String username, String password, String sql, @RequestParam(required = false)Integer page, @RequestParam(required = false)Integer pageSize) {
//        dynamicFilenetService.queryFilenet(url,username,password,sql,page,pageSize);
//        return dynamicFilenetService.getFilenetPage();
//    }
//
//    @RequestMapping(value ="/download",method = RequestMethod.POST,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
//    public void downloadDocument(String url, String username, String password, String docId, HttpServletResponse response) {
//        dynamicFilenetService.downloadDocumentStream(url,username,password,docId,response);
//    }
//
//    @PostConstruct
//    public void postConstruct() {
//
//    }
//
//    @Autowired
//    public FilenetRestController(DynamicFilenetService dynamicFilenetService) {
//        this.dynamicFilenetService = dynamicFilenetService;
//    }
//}
