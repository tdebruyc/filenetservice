/**
	IBM grants you a nonexclusive copyright license to use all programming code
	examples from which you can generate similar function tailored to your own
	specific needs.

	All sample code is provided by IBM for illustrative purposes only.
	These examples have not been thoroughly tested under all conditions.  IBM,
	therefore cannot guarantee or imply reliability, serviceability, or function of
	these programs.

	All Programs or code component contained herein are provided to you �AS IS �
	without any warranties of any kind.
	The implied warranties of non-infringement, merchantability and fitness for a
	particular purpose are expressly disclaimed.

	� Copyright IBM Corporation 2007, ALL RIGHTS RESERVED.
 */

package gov.mn.commerce.efiling.filenet;

import com.filenet.api.collection.ObjectStoreSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.*;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;

import javax.security.auth.Subject;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

/**
 * This object represents the connection with
 * the Content Engine. Once connection is established
 * it intializes Domain and ObjectStoreSet with
 * available Domain and ObjectStoreSet.
 *
 */
public class FileNetCEConnection implements Serializable
{
	private Connection con;
	private Domain dom;
	private String domainName;
	private ObjectStoreSet ost;
	private Vector osnames;
	private boolean isConnected;
	private UserContext uc;


	/*
	 * constructor
	 */
	public FileNetCEConnection()
	{
		con = null;
		uc = UserContext.get();
		dom = null;
		domainName = null;
		ost = null;
		osnames = new Vector();
		isConnected = false;
	}

	/*
	 * Establishes connection with Content Engine using
	 * supplied username, password, JAAS stanza and CE Uri.
	 */
	public void establishConnection(String userName, String password, String stanza, String uri) {
		con = Factory.Connection.getConnection(uri);
		Subject sub = UserContext.createSubject(con,userName,password,stanza);
		uc.pushSubject(sub);
		//try to use getDomain here or exclude properties to only what we need
		dom = getDomain();
		isConnected = true;
	}

	/*
	 * Returns Domain object.
	 */
	public Domain fetchDomain()
	{
		dom = Factory.Domain.fetchInstance(con, null, null);
		return dom;
	}

	public Domain getDomain() {
		dom = Factory.Domain.getInstance(con, null);
		return dom;
	}

	/*
     * Returns ObjectStoreSet from Domain
     */
	public ObjectStoreSet getOSSet()
	{
		ost = dom.get_ObjectStores();
		return ost;
	}

	/*
     * Returns vector containing ObjectStore
     * names from object stores available in
     * ObjectStoreSet.
     */
	public Vector getOSNames()
	{
		if(osnames.isEmpty())
		{
			Iterator it = ost.iterator();
			while(it.hasNext())
			{
				ObjectStore os = (ObjectStore) it.next();
				osnames.add(os.get_DisplayName());
			}
		}
		return osnames;
	}

	/*
	 * Checks whether connection has established
	 * with the Content Engine or not.
	 */
	public boolean isConnected()
	{
		return isConnected;
	}

	/*
	 * Returns ObjectStore object for supplied
	 * object store name.
	 */
	public ObjectStore fetchOS(String name) {
		ObjectStore os = Factory.ObjectStore.fetchInstance(dom, name, null);
		return os;
	}

	public ObjectStore fetchOSWithIdOnly(String name){
		PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(0, null, null,PropertyNames.ID,null));
		ObjectStore os = Factory.ObjectStore.fetchInstance(dom, name, pf);
		return os;
	}

	public ObjectStore getOS(String name) {
		ObjectStore os = Factory.ObjectStore.getInstance(dom, name);
		return os;
	}

	public ObjectStore fetchOSbyId(String id)
	{
		ObjectStore os = Factory.ObjectStore.fetchInstance(dom,new Id(id),null);
		return os;
	}

	public Document fetchDocument(ObjectStore os, String id, String... props){
		PropertyFilter pf = new PropertyFilter();
		for(String s : props){
			pf.addIncludeProperty(new FilterElement(0,null,null,s,null));
		}
		return Factory.Document.fetchInstance(os,id,pf);
	}

	public Document fetchDocument(ObjectStore os, String id){
		return Factory.Document.fetchInstance(os,id,new PropertyFilter());
	}


	/*
	 * Returns the domain name.
	 */
	public String getDomainName()
	{
		return domainName;
	}

	public void finishUpAndPopSubject(){
		uc.popSubject();
	}

}
