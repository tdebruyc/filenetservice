package gov.mn.commerce.efiling.filenet;

import com.filenet.api.collection.PageIterator;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.query.RepositoryRow;
import com.filenet.api.query.SearchScope;
import com.filenet.api.query.SearchSQL;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.springframework.stereotype.Service;

/**
 * Created by dstewart on 11/17/2016.
 */
@Service
@Log4j
public class DynamicFilenetService implements Serializable {
    private PageIterator iterator;
    private FilenetPage filenetPage;
    private FileNetCEConnection connection;

    public FilenetPage getFilenetPage() {
        return filenetPage;
    }

    public void queryFilenet(
        String url,
        String username,
        String password,
        String sql,
        Integer page,
        Integer pageSize
    ) {
        connection = new FileNetCEConnection();
        List<CommerceFilenetDocument> commerceFilenetDocumentList = new ArrayList<CommerceFilenetDocument>();
        try {
            connection.establishConnection(
                username,
                password,
                "FileNetP8WSI",
                url
            );
            ObjectStore os = connection.fetchOSWithIdOnly("Commerce");

            //            os.set_DefaultQueryTimeLimit(90);
            String osId = os.get_Id().toString();
            RepositoryRowSet repositoryRowSet = new SearchScope(os)
            .fetchRows(new SearchSQL(sql), (pageSize), null, true);
            iterator = repositoryRowSet.pageIterator();
            boolean hasNext = false;

            //Disabled for now (always returns null) since FileNet total count appears to be really inefficient
            Integer elementCount = null;
            Integer pageSizeFromCE = null;
            if (!repositoryRowSet.isEmpty()) {
                for (int x = 0; x < ((page != null) ? page : 1); x++) {
                    iterator.nextPage();
                }
                for (Object o : iterator.getCurrentPage()) {
                    RepositoryRow row = (RepositoryRow) o;

                    //row.getProperties() should have everything
                    //document properties are already in repositoryRow--no need to fetch document again
                    commerceFilenetDocumentList.add(
                        new CommerceFilenetDocument(row)
                    );
                }
                pageSizeFromCE = iterator.getElementCount();
                hasNext =
                    (iterator.getPageSize() == iterator.getElementCount());
            }
            filenetPage =
                new FilenetPage(
                    hasNext,
                    page != null && (page > 1),
                    page,
                    pageSizeFromCE,
                    null,
                    commerceFilenetDocumentList
                );
        } catch (Exception e) {
            filenetPage = new FilenetPage(e);
        } finally {
            connection.finishUpAndPopSubject();
        }
    }

    private static String ALLOW_DOC_CLASS_NAME = "Dockets Public";

    public void downloadDocumentStream(
        String url,
        String username,
        String password,
        String docId,
        HttpServletResponse response
    ) {
        try {
            connection = new FileNetCEConnection();
            connection.establishConnection(
                username,
                password,
                "FileNetP8WSI",
                url
            );
            ObjectStore os = connection.fetchOSWithIdOnly("Commerce");
            String[] props = {
                PropertyNames.CONTENT_ELEMENTS,
                PropertyNames.NAME,
                PropertyNames.MIME_TYPE,
                PropertyNames.CLASS_DESCRIPTION
            };
            Document doc = connection.fetchDocument(os, docId, props);
            if (
                doc
                    .get_ClassDescription()
                    .get_Name()
                    .equalsIgnoreCase(ALLOW_DOC_CLASS_NAME)
            ) {
                response.setContentType(doc.get_MimeType());
                MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
                MimeType mime = allTypes.forName(doc.get_MimeType());
                response.setHeader(
                    "Content-Disposition",
                    "inline;filename=" + doc.get_Name() + mime.getExtension()
                );
                IOUtils.copy(
                    doc.accessContentStream(0),
                    response.getOutputStream()
                );
                response.flushBuffer();
            }
        } catch (MimeTypeException me) {
            log.error(me);
        } catch (IOException ignored) {
            connection.finishUpAndPopSubject();
        } catch (EngineRuntimeException ere) {
            log.error("Error downloading document from FileNet: ", ere);
            connection.finishUpAndPopSubject();
        } finally {
            connection.finishUpAndPopSubject();
        }
    }

    private String[] docketDocumentProperties = {
        PropertyNames.MIME_TYPE,
        PropertyNames.CONTENT_SIZE,
        PropertyNames.NAME,
        PropertyNames.DATE_CREATED,
        PropertyNames.DATE_LAST_MODIFIED,
        PropertyNames.CLASS_DEFINITION,
        "DocumentDate",
        "AdditionalInfo",
        "DocketID",
        "Submitter",
        "SubmitNumber",
        "OnBehalfof",
        "DocketType",
        "DocumentType"
    };
}
