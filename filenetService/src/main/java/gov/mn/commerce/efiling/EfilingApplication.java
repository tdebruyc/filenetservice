package gov.mn.commerce.efiling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@CrossOrigin(origins = "*")
@SpringBootApplication
public class EfilingApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfilingApplication.class, args);
	}

}
