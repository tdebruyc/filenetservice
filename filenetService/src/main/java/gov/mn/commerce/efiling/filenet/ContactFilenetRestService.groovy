package filenet

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Log4j
import groovy.util.logging.Slf4j
import org.apache.commons.io.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletResponse

/**
 * Created by dstewart on 5/15/2017.
 */

@Component
@Log4j
class ContactFilenetRestService implements Serializable{

    @Autowired
    private DynamicFilenetService dynamicFilenetService

    private static Integer connectTimeout = 90000
    private static Integer readTimeout = 90000
//    private static String username = databaseProperties.getProperty("commerce.filenet.service.user")
//    private static String password = databaseProperties.getProperty("commerce.filenet.service.password")
//    private String serviceRoot = databaseProperties.getProperty("filenet.portlet.root")
//    private static String credentials = "$username:$password".bytes.encodeBase64().toString()
//    private static String credentialsValue = "Basic $credentials"

    FilenetPage connect(String sql) {
        try {
            String url = "${DatabaseProperties.dbProperties.getProperty("filenet.portlet.root")}/rest/filenet/query"
            HttpURLConnection conn = restPostConnector(
                    url,
                    [
                            "username": DatabaseProperties.dbProperties.getProperty("filenet.dockets.username"),
                            "password": DatabaseProperties.dbProperties.getProperty("filenet.dockets.password"),
                            "url"     : DatabaseProperties.dbProperties.getProperty("filenet.url"),
                            "sql"     : URLEncoder.encode(sql, "UTF-8")
                    ]
            )
            String response = conn.inputStream.withReader { Reader reader -> reader.text }
            return new ObjectMapper().readValue(response, FilenetPage.class)
        } catch (Exception e) {
            log.error("Error in FileNet connect call: ", e)
        }
        return null
    }

    void download(String documentId, HttpServletResponse response) {
        try {
            String url = "${DatabaseProperties.dbProperties.getProperty("filenet.portlet.root")}/rest/filenet/download"
            HttpURLConnection conn = restPostConnector(
                    url,
                    [
                            "username": DatabaseProperties.dbProperties.getProperty("filenet.dockets.username"),
                            "password": DatabaseProperties.dbProperties.getProperty("filenet.dockets.password"),
                            "url"     : DatabaseProperties.dbProperties.getProperty("filenet.url"),
                            "docId"   : documentId
                    ]
            )
            conn.getContentType()
            response.setContentType(conn.getContentType())
            response.setHeader("Content-Disposition", conn.getHeaderField("Content-Disposition"))
            IOUtils.copy(conn.getInputStream(), response.getOutputStream())
        } catch (Exception e) {
            log.error("Error in FileNet download call: ", e)
        }
    }

    FilenetPage connectTemp(String sql) {
        return queryFilenet(DatabaseProperties.dbProperties.getProperty("filenet.url"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.username"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.password"), sql, null, null)
    }

    FilenetPage connectTempWithPaginator(String sql, Integer page, Integer pageSize) {
        return queryFilenet(DatabaseProperties.dbProperties.getProperty("filenet.url"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.username"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.password"), sql, page, pageSize)
    }

    void downloadTemp(String documentId, HttpServletResponse response) {
        downloadDocument(DatabaseProperties.dbProperties.getProperty("filenet.url"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.username"),
                DatabaseProperties.dbProperties.getProperty("filenet.dockets.password"),
                documentId, response)
    }

    private static HttpURLConnection restPostConnector(String connectUrl, Map params) {
        HttpURLConnection conn = connectUrl.toURL().openConnection()
        conn.setDoOutput(true)
        conn.setConnectTimeout(connectTimeout)
        conn.setReadTimeout(readTimeout)
        conn.setRequestMethod("POST")
//        conn.setRequestProperty("Authorization",credentialsValue)
        conn.outputStream.withWriter { Writer writer ->
            //be careful about passing in query directly
            params.eachWithIndex { it, idx ->
                if (idx != 0) writer << "&"
                writer << "${it.key}=${it.value}"
            }
        }
        return conn
    }

    private FilenetPage queryFilenet(String url, String username, String password, String sql, Integer page, Integer pageSize) {
        dynamicFilenetService.queryFilenet(url, username, password, sql, page, pageSize)
        return dynamicFilenetService.getFilenetPage()
    }


    private void downloadDocument(String url, String username, String password, String docId, HttpServletResponse response) {
        dynamicFilenetService.downloadDocumentStream(url, username, password, docId, response)
    }

}
