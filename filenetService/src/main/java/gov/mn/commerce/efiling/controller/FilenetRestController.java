package gov.mn.commerce.efiling.controller;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.rest.webmvc.support.RepositoryConstraintViolationExceptionMessage.ValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import gov.mn.commerce.efiling.filenet.DatabaseProperties;
import gov.mn.commerce.efiling.filenet.DynamicFilenetService;
import gov.mn.commerce.efiling.filenet.FilenetPage;
import gov.mn.commerce.efiling.model.DocketSearchModel;
import gov.mn.commerce.efiling.util.JSONDataUtil;
import lombok.extern.log4j.Log4j;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/filenet")
@Log4j
public class FilenetRestController implements Serializable {

	private static final long serialVersionUID = 2197174920459872721L;
	private DynamicFilenetService dynamicFilenetService;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage testprops() {
		Properties props = DatabaseProperties.getDbProperties();

		// JSONDataUtil.printAsJSON("Props", props);
		String url = GetValue("filenet.url", props);
		String userName = GetValue("filenet.dockets.username.dev", props);
		String password = GetValue("filenet.dockets.password.dev", props);

		String query = "SELECT TOP 100 DocumentDate,ReceivedDate,DocketID,OnBehalfof,AdditionalInfo,Name,Id,DocumentTitle,"
				+ "MimeType,DocumentType FROM DocketsPublic d WHERE d.[DocketID] LIKE '19-%' ORDER BY d.[DocketID]";

		// String query = "SELECT DocumentTitle FROM Document WHERE DocumentTitle LIKE
		// 'A%' ORDER BY DocumentTitle";

		return queryFilenet(url, userName, password, query, 1, 100);
	}

	@CrossOrigin(origins = "http://localhost:4096")
	@PostMapping(path = "/docketsearch/docketyear/{docketyear}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage docketSearch(@PathVariable String docketyear) {
		Properties props = DatabaseProperties.getDbProperties();

		// JSONDataUtil.printAsJSON("Props", props);
		String url = GetValue("filenet.url", props);
		String userName = GetValue("filenet.dockets.username.dev", props);
		String password = GetValue("filenet.dockets.password.dev", props);
		//String query = "SELECT TOP 100 DocumentDate,ReceivedDate,DocketID,OnBehalfof,AdditionalInfo,Name,Id,DocumentTitle,"
			//	+ "MimeType,DocumentType FROM DocketsPublic d WHERE d.[DocketID] LIKE '" + docketyear
//				+ "-%' ORDER BY d.[DocketID]";

		String query = "SELECT TOP 100 d.*, d.DocumentDate FROM DocketsPublic d WHERE d.[DocketID] LIKE '19-%'";
		/*
		String query = "select tabschema concat '.' concat tabname as table_name,\r\n" + 
				"    card as rows, \r\n" + 
				"    stats_time\r\n" + 
				"from syscat.tables\r\n" + 
				"order by card desc";
				*/

		return queryFilenet(url, userName, password, query, 1, 100);
	}

	/**
	 * Works
	 * @param docketSearchModel
	 * @return
	 */
	@CrossOrigin(origins = "http://localhost:4096")
	@PostMapping(path = "/docketsearch/dockets/clause/{docketSearchModel}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String testSearch(@PathVariable JSONObject docketSearchModel) {
		System.out.println("##################### in clause " + docketSearchModel);
		DocketSearchModel myDocketSearchModel = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			myDocketSearchModel = mapper.readValue(docketSearchModel.toString(), DocketSearchModel.class);
			System.out.println("$$$$$$$$$$$$$$$$$$$$ DocketSearchModel: " + myDocketSearchModel);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Properties props = DatabaseProperties.getDbProperties();

		return "OK";
	}

	/**
	 * Query uses multiple criteria
	 * @param jsonObject
	 * @return
	 * @throws Exception
	 */
	
	@CrossOrigin(origins = "http://localhost:4096")
	@PostMapping(path = "/docketsearch/multiclause/{jsonObject}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage searchMultiClause(@PathVariable JSONObject jsonObject) throws Exception {

		DocketSearchModel docketSearchModel = loadSearchModel(jsonObject);
		System.out.println("### model: "+docketSearchModel);

		Properties props = DatabaseProperties.getDbProperties();
		JSONDataUtil.printAsJSON("Props", props);
		
		String url = GetValue("filenet.url", props);
		String userName = GetValue("filenet.dockets.username.dev", props);
		String password = GetValue("filenet.dockets.password.dev", props);
		
		String query = "SELECT TOP 100 DocumentDate,ReceivedDate,DocketID,DocketType,OnBehalfof,AdditionalInfo,Name,Id,DocumentTitle,"
				+ "MimeType,DocumentType FROM DocketsPublic d "
				+ "WHERE d.[DocketID] LIKE '" + docketSearchModel.getDocketYear()
				+ "-%' ORDER BY d.[DocketID]";
		
		return queryFilenet(url, userName, password, query, 1, 100);
	}

	@RequestMapping(value = "/**", method = RequestMethod.OPTIONS)
	public ResponseEntity handle() {
		System.out.println("######################## hit handle");
		return new ResponseEntity(HttpStatus.OK);
	}

	public String GetValue(String key, Properties properties) {
		return properties.getProperty(key);
	}

	@RequestMapping(value = "/query", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public FilenetPage queryFilenet(String url, String username, String password, String sql,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize) {
		dynamicFilenetService.queryFilenet(url, username, password, sql, page, pageSize);
		return dynamicFilenetService.getFilenetPage();
	}

	@RequestMapping(value = "/download", method = RequestMethod.POST, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void downloadDocument(String url, String username, String password, String docId,
			HttpServletResponse response) {
		dynamicFilenetService.downloadDocumentStream(url, username, password, docId, response);
	}

	private DocketSearchModel loadSearchModel(JSONObject jsonObject) {
		DocketSearchModel docketSearchModel = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			docketSearchModel = mapper.readValue(jsonObject.toString(), DocketSearchModel.class);
			System.out.println("@@@@@@@@@@@@@@ loadSearchModel DocketSearchModel: " + docketSearchModel);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return docketSearchModel; 
	}
	
	@PostConstruct
	public void postConstruct() {

	}

	@Autowired
	public FilenetRestController(DynamicFilenetService dynamicFilenetService) {
		this.dynamicFilenetService = dynamicFilenetService;
	}
}
