package gov.mn.commerce.efiling.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class DocketSearchModel implements Serializable {

	private static final long serialVersionUID = 1971390403793174715L;

	// Basic Search criteria
	private Integer  docketYear;
	private String docketNumber;
	private String dockumentTypeString;
	private String docketType;
	private String docketId;
	private String submissionNumber;
	private String onBehalfOf;
	private Date receivedDate;
	private Date receivedStartDate;
	private Date receivedEndDate;
	
	// Advanced Keyword Search
	private String keywords;
	
	// Result Options
	private String sortCriteria;
	private Integer resultsPerPage;
	public Integer getDocketYear() {
		return docketYear;
	}
	public void setDocketYear(Integer docketYear) {
		this.docketYear = docketYear;
	}
	public String getDocketNumber() {
		return docketNumber;
	}
	public void setDocketNumber(String docketNumber) {
		this.docketNumber = docketNumber;
	}
	public String getDockumentTypeString() {
		return dockumentTypeString;
	}
	public void setDockumentTypeString(String dockumentTypeString) {
		this.dockumentTypeString = dockumentTypeString;
	}
	public String getDocketType() {
		return docketType;
	}
	public void setDocketType(String docketType) {
		this.docketType = docketType;
	}
	public String getDocketId() {
		return docketId;
	}
	public void setDocketId(String docketId) {
		this.docketId = docketId;
	}
	public String getSubmissionNumber() {
		return submissionNumber;
	}
	public void setSubmissionNumber(String submissionNumber) {
		this.submissionNumber = submissionNumber;
	}
	public String getOnBehalfOf() {
		return onBehalfOf;
	}
	public void setOnBehalfOf(String onBehalfOf) {
		this.onBehalfOf = onBehalfOf;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public Date getReceivedStartDate() {
		return receivedStartDate;
	}
	public void setReceivedStartDate(Date receivedStartDate) {
		this.receivedStartDate = receivedStartDate;
	}
	public Date getReceivedEndDate() {
		return receivedEndDate;
	}
	public void setReceivedEndDate(Date receivedEndDate) {
		this.receivedEndDate = receivedEndDate;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getSortCriteria() {
		return sortCriteria;
	}
	public void setSortCriteria(String sortCriteria) {
		this.sortCriteria = sortCriteria;
	}
	public Integer getResultsPerPage() {
		return resultsPerPage;
	}
	public void setResultsPerPage(Integer resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}
	@Override
	public int hashCode() {
		return Objects.hash(docketId, docketNumber, docketType, docketYear, dockumentTypeString, keywords, onBehalfOf,
				receivedDate, receivedEndDate, receivedStartDate, resultsPerPage, sortCriteria, submissionNumber);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof DocketSearchModel)) {
			return false;
		}
		DocketSearchModel other = (DocketSearchModel) obj;
		return Objects.equals(docketId, other.docketId) && Objects.equals(docketNumber, other.docketNumber)
				&& Objects.equals(docketType, other.docketType) && Objects.equals(docketYear, other.docketYear)
				&& Objects.equals(dockumentTypeString, other.dockumentTypeString)
				&& Objects.equals(keywords, other.keywords) && Objects.equals(onBehalfOf, other.onBehalfOf)
				&& Objects.equals(receivedDate, other.receivedDate)
				&& Objects.equals(receivedEndDate, other.receivedEndDate)
				&& Objects.equals(receivedStartDate, other.receivedStartDate)
				&& Objects.equals(resultsPerPage, other.resultsPerPage)
				&& Objects.equals(sortCriteria, other.sortCriteria)
				&& Objects.equals(submissionNumber, other.submissionNumber);
	}
	@Override
	public String toString() {
		return "DocketSearchModel [docketYear=" + docketYear + ", docketNumber=" + docketNumber
				+ ", dockumentTypeString=" + dockumentTypeString + ", docketType=" + docketType + ", docketId="
				+ docketId + ", submissionNumber=" + submissionNumber + ", onBehalfOf=" + onBehalfOf + ", receivedDate="
				+ receivedDate + ", receivedStartDate=" + receivedStartDate + ", receivedEndDate=" + receivedEndDate
				+ ", keywords=" + keywords + ", sortCriteria=" + sortCriteria + ", resultsPerPage=" + resultsPerPage
				+ "]";
	}
	
	
}
