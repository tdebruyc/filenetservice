package gov.mn.commerce.efiling.filenet;

import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.DatabaseConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by dstewart on 4/27/2017.
 */

@Component
public class DatabaseProperties implements Serializable{

	private static final long serialVersionUID = 9080834710258610364L;

	@Autowired
    private DataSource eeraSource;

    private static Properties dbProperties;

    public static Properties getDbProperties() {
        return dbProperties;
    }

    public void loadFromDatabase(){
        DatabaseConfiguration config = new DatabaseConfiguration(eeraSource,"eera_setting","setting_key","setting_value");
        dbProperties = ConfigurationConverter.getProperties(config);
    }

    @PostConstruct
    public void postConstruct(){
        loadFromDatabase();
    }
}
