package gov.mn.commerce.efiling.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JSONDataUtil {

    /**
     * This is a DEBUG method that will be moved to a utility class.
     * @param objLabel The label to use for the object being rendered.
     * @param object The object to render.
     */
    public static void printAsJSON(String objLabel, Object object) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        try {
            String data = mapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(object);
            System.out.println(
                "\n\n[#############################################################\n" +
                objLabel +
                " " +
                data +
                "\n#############################################################]\n"
            );
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
